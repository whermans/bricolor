import configparser


def get_key():
    cfg = configparser.ConfigParser()
    cfg.read('config.ini')
    return cfg['secret']['key']
