import colorsys
import math


class ValidColor:

    def __init__(self, name, hx):
        self.name = name
        self.rgb = hex_to_rgb(hx)
        self.hx = hx
        red, green, blue = self.rgb
        self.hls = rgb_to_hls(red, green, blue)

    rgb = None
    hx = None
    hls = None
    name = None

    def debug_print(self):
        print('{0}: {1} {2} {3}'.format(self.name, self.rgb, self.hx, self.hls))


def hex_to_rgb(hx):
    return tuple(int(hx[i:i + 2], 16) for i in (0, 2, 4))


def rgb_to_hex(rgb):
    r, g, b = rgb
    return '#{:02x}{:02x}{:02x}'.format(r, g, b)


def rgb_to_hls(red, green, blue):
    red, green, blue = [x / 255.0 for x in (red, green, blue)]
    hue, lightness, saturation = colorsys.rgb_to_hls(red, green, blue)
    hue = int(hue * 360)
    lightness = int(lightness * 100)
    saturation = int(saturation * 100)
    return hue, lightness, saturation


def hls_to_rgb(hue, lightness, saturation):
    hue, lightness, saturation = [x / 360.0 if i == 0 else x / 100.0 for i, x in
                                  enumerate((hue, lightness, saturation))]
    red, green, blue = colorsys.hls_to_rgb(hue, lightness, saturation)
    red, green, blue = [int(x * 255) for x in (red, green, blue)]
    return red, green, blue


def color_diff(hls1, hls2):
    diff = math.sqrt(sum((a - b) ** 2 for a, b in zip(hls1, hls2)))
    return diff
