import requests
import time

from config import get_key
from valid_color import ValidColor

key = get_key()
base = 'https://rebrickable.com/api/v3'


def call(address):
    address = base + address
    print(address)
    time.sleep(1)
    res = requests.get(address)
    if res.status_code == 200:
        return res.json()
    else:
        print('Call failed: {}'.format(res.status_code))
    return None


def get_colors():
    res = []
    raw = call('/lego/colors?key=' + key)
    if raw is not None:
        colors = raw['results']
        for val in colors:
            name = val['name']
            rgb = val['rgb']
            valid_color = ValidColor(name, rgb)
            valid_color.debug_print()
            res.append(valid_color)
    return res
