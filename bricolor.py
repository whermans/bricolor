from picture import get_pic, get_pixel, set_pixel, save_matched
from rebrickable import get_colors
from valid_color import color_diff, rgb_to_hls
from sys import maxsize

fn = 'test.png'


def main():
    valid_colors = get_colors()
    pic = get_pic(fn)
    for x in range(pic.scaled.width):
        print('Column', x)
        for y in range(pic.scaled.height):
            px = get_pixel(pic, (x, y))
            closest = None
            closest_diff = maxsize
            for c in valid_colors:
                if len(px) == 3:
                    red, green, blue = px
                else:
                    red, green, blue, alpha = px
                diff = color_diff(rgb_to_hls(red, green, blue), c.hls)
                if closest_diff > diff:
                    closest_diff = diff
                    closest = c
            print('Row', y, ':', closest.name)
            set_pixel(pic, (x, y), closest.rgb)
    save_matched(pic)


if __name__ == '__main__':
    main()
