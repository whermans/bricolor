from PIL import Image


class Pic:

    def __init__(self):
        pass

    original = None
    original_dimensions = None
    scaled = None
    color_matched = None

    def debug_print(self):
        print('\nImage loaded and resized: {} -> {}'
              .format(str(self.original.size), str(self.scaled.size)))


def get_pic(fn):
    pic = Pic()
    pic.original = Image.open(fn)
    pic.original_dimensions = pic.original.size
    pic.matched = Image.open(fn)
    pic.scaled = Image.open(fn)
    pic.debug_print()
    return pic


def get_pixel(pic, coords):
    pixels = pic.scaled.load()
    return pixels[coords]


def set_pixel(pic, coords, val):
    pic.matched.putpixel(coords, val)


def save_matched(pic):
    pic.matched.resize((64, 64), resample=Image.Resampling.NEAREST).resize(pic.original_dimensions,
                                                                           Image.Resampling.NEAREST).save('matched.png')
